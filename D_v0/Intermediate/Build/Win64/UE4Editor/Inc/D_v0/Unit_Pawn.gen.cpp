// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "D_v0/Unit_Pawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUnit_Pawn() {}
// Cross Module References
	D_V0_API UEnum* Z_Construct_UEnum_D_v0_EFaction();
	UPackage* Z_Construct_UPackage__Script_D_v0();
	D_V0_API UEnum* Z_Construct_UEnum_D_v0_EType();
	D_V0_API UClass* Z_Construct_UClass_AUnit_Pawn_NoRegister();
	D_V0_API UClass* Z_Construct_UClass_AUnit_Pawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeArmor();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_SetAttribute();
	D_V0_API UFunction* Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute();
// End Cross Module References
	static UEnum* EFaction_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_D_v0_EFaction, Z_Construct_UPackage__Script_D_v0(), TEXT("EFaction"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFaction(EFaction_StaticEnum, TEXT("/Script/D_v0"), TEXT("EFaction"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_D_v0_EFaction_CRC() { return 1331076256U; }
	UEnum* Z_Construct_UEnum_D_v0_EFaction()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_D_v0();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFaction"), 0, Get_Z_Construct_UEnum_D_v0_EFaction_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFaction::Faction_Dire", (int64)EFaction::Faction_Dire },
				{ "EFaction::Faction_Radiant", (int64)EFaction::Faction_Radiant },
				{ "EFaction::Faction_Neutral", (int64)EFaction::Faction_Neutral },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Faction_Dire.DisplayName", "Dire" },
				{ "Faction_Neutral.DisplayName", "Neutral" },
				{ "Faction_Radiant.DisplayName", "Radiant" },
				{ "ModuleRelativePath", "Unit_Pawn.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_D_v0,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EFaction",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EFaction",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_D_v0_EType, Z_Construct_UPackage__Script_D_v0(), TEXT("EType"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EType(EType_StaticEnum, TEXT("/Script/D_v0"), TEXT("EType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_D_v0_EType_CRC() { return 2046728115U; }
	UEnum* Z_Construct_UEnum_D_v0_EType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_D_v0();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EType"), 0, Get_Z_Construct_UEnum_D_v0_EType_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EType::Type_Strength", (int64)EType::Type_Strength },
				{ "EType::Type_Agility", (int64)EType::Type_Agility },
				{ "EType::Type_Intelligence", (int64)EType::Type_Intelligence },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Unit_Pawn.h" },
				{ "Type_Agility.DisplayName", "Agility" },
				{ "Type_Intelligence.DisplayName", "Intelligence" },
				{ "Type_Strength.DisplayName", "Strength" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_D_v0,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"EType",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"EType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void AUnit_Pawn::StaticRegisterNativesAUnit_Pawn()
	{
		UClass* Class = AUnit_Pawn::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ComputeArmor", &AUnit_Pawn::execComputeArmor },
			{ "ComputeAttackDamage", &AUnit_Pawn::execComputeAttackDamage },
			{ "ComputeDamageMultiplier", &AUnit_Pawn::execComputeDamageMultiplier },
			{ "ComputeEffectiveHP", &AUnit_Pawn::execComputeEffectiveHP },
			{ "ComputeHealthRegen", &AUnit_Pawn::execComputeHealthRegen },
			{ "ComputeManaRegen", &AUnit_Pawn::execComputeManaRegen },
			{ "ComputeMaxHealth", &AUnit_Pawn::execComputeMaxHealth },
			{ "ComputeMaxMana", &AUnit_Pawn::execComputeMaxMana },
			{ "ComputeTurnRate", &AUnit_Pawn::execComputeTurnRate },
			{ "SetAttribute", &AUnit_Pawn::execSetAttribute },
			{ "SetPrimaryAttribute", &AUnit_Pawn::execSetPrimaryAttribute },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeArmor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeArmor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeArmor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeArmor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeArmor_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeArmor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeArmor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeArmor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeAttackDamage", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier_Statics::Function_MetaDataParams[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeDamageMultiplier", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP_Statics::Function_MetaDataParams[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeEffectiveHP", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeHealthRegen", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mana Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeManaRegen", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeMaxHealth", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mana" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeMaxMana", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics
	{
		struct Unit_Pawn_eventComputeTurnRate_Parms
		{
			float modifier;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_modifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::NewProp_modifier = { UE4CodeGen_Private::EPropertyClass::Float, "modifier", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(Unit_Pawn_eventComputeTurnRate_Parms, modifier), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::NewProp_modifier,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Turn Rate" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "ComputeTurnRate", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Unit_Pawn_eventComputeTurnRate_Parms), Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_SetAttribute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_SetAttribute_Statics::Function_MetaDataParams[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
		{ "ToolTip", "FUNCTIONS                                       //UFUNCTION(BlueprintCallable, Category = \"Armor\")\n//     void ComputeMainArmor();" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_SetAttribute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "SetAttribute", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_SetAttribute_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_SetAttribute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_SetAttribute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_SetAttribute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute_Statics::Function_MetaDataParams[] = {
		{ "Category", "Primary Attribute" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUnit_Pawn, "SetPrimaryAttribute", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AUnit_Pawn_NoRegister()
	{
		return AUnit_Pawn::StaticClass();
	}
	struct Z_Construct_UClass_AUnit_Pawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TurnRateModifiers_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TurnRateModifiers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DamageMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DamageMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EffectiveHP_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EffectiveHP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusArmor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusArmor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusArmorAgility_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusArmorAgility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseArmor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseArmor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Armor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Armor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusManaRegenItems_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusManaRegenItems;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusManaRegenIntelligence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusManaRegenIntelligence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseManaRegen_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseManaRegen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManaRegen_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ManaRegen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusManaItems_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusManaItems;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusManaIntelligence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusManaIntelligence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentMana_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentMana;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxMana_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxMana;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseMana_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseMana;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusHealthRegenItems_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusHealthRegenItems;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusHealthRegenStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusHealthRegenStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseHealthRegen_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseHealthRegen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealthRegen_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealthRegen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusHealthItems_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusHealthItems;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusHealthStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusHealthStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseHealth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseHealth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseAttackDamage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseAttackDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AbilityBonuses_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AbilityBonuses;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinMaxGap_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinMaxGap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MainAttackDamage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MainAttackDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaximumAttackDamage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaximumAttackDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinimumAttackDamage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinimumAttackDamage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryAttributePoints_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PrimaryAttributePoints;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusIntelligence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusIntelligence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusAgility_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusAgility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseIntelligence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseIntelligence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseAgility_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseAgility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Intelligence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Intelligence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Agility_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Agility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Faction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Faction;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Faction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUnit_Pawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_D_v0,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AUnit_Pawn_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeArmor, "ComputeArmor" }, // 3432389919
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeAttackDamage, "ComputeAttackDamage" }, // 1657696717
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeDamageMultiplier, "ComputeDamageMultiplier" }, // 2099252438
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeEffectiveHP, "ComputeEffectiveHP" }, // 3966766113
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeHealthRegen, "ComputeHealthRegen" }, // 3866367137
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeManaRegen, "ComputeManaRegen" }, // 398880709
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeMaxHealth, "ComputeMaxHealth" }, // 227244107
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeMaxMana, "ComputeMaxMana" }, // 2719384656
		{ &Z_Construct_UFunction_AUnit_Pawn_ComputeTurnRate, "ComputeTurnRate" }, // 3029322784
		{ &Z_Construct_UFunction_AUnit_Pawn_SetAttribute, "SetAttribute" }, // 3559540321
		{ &Z_Construct_UFunction_AUnit_Pawn_SetPrimaryAttribute, "SetPrimaryAttribute" }, // 306814775
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Unit_Pawn.h" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRateModifiers_MetaData[] = {
		{ "Category", "Turn Rate" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRateModifiers = { UE4CodeGen_Private::EPropertyClass::Float, "TurnRateModifiers", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, TurnRateModifiers), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRateModifiers_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRateModifiers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Turn Rate" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseTurnRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseTurnRate", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRate_MetaData[] = {
		{ "Category", "Turn Rate" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRate = { UE4CodeGen_Private::EPropertyClass::Float, "TurnRate", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, TurnRate), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRate_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_DamageMultiplier_MetaData[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_DamageMultiplier = { UE4CodeGen_Private::EPropertyClass::Float, "DamageMultiplier", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, DamageMultiplier), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_DamageMultiplier_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_DamageMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_EffectiveHP_MetaData[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_EffectiveHP = { UE4CodeGen_Private::EPropertyClass::Float, "EffectiveHP", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, EffectiveHP), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_EffectiveHP_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_EffectiveHP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmor_MetaData[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmor = { UE4CodeGen_Private::EPropertyClass::Float, "BonusArmor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusArmor), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmor_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmorAgility_MetaData[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmorAgility = { UE4CodeGen_Private::EPropertyClass::Float, "BonusArmorAgility", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusArmorAgility), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmorAgility_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmorAgility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseArmor_MetaData[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseArmor = { UE4CodeGen_Private::EPropertyClass::Float, "BaseArmor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseArmor), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseArmor_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseArmor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Armor_MetaData[] = {
		{ "Category", "Armor" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Armor = { UE4CodeGen_Private::EPropertyClass::Float, "Armor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, Armor), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Armor_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Armor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenItems_MetaData[] = {
		{ "Category", "Mana Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenItems = { UE4CodeGen_Private::EPropertyClass::Float, "BonusManaRegenItems", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusManaRegenItems), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenItems_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenItems_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenIntelligence_MetaData[] = {
		{ "Category", "Mana Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenIntelligence = { UE4CodeGen_Private::EPropertyClass::Float, "BonusManaRegenIntelligence", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusManaRegenIntelligence), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenIntelligence_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenIntelligence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseManaRegen_MetaData[] = {
		{ "Category", "Mana Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseManaRegen = { UE4CodeGen_Private::EPropertyClass::Float, "BaseManaRegen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseManaRegen), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseManaRegen_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseManaRegen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_ManaRegen_MetaData[] = {
		{ "Category", "Mana Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_ManaRegen = { UE4CodeGen_Private::EPropertyClass::Float, "ManaRegen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, ManaRegen), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_ManaRegen_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_ManaRegen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaItems_MetaData[] = {
		{ "Category", "Mana" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaItems = { UE4CodeGen_Private::EPropertyClass::Float, "BonusManaItems", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusManaItems), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaItems_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaItems_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaIntelligence_MetaData[] = {
		{ "Category", "Mana" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaIntelligence = { UE4CodeGen_Private::EPropertyClass::Float, "BonusManaIntelligence", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusManaIntelligence), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaIntelligence_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaIntelligence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentMana_MetaData[] = {
		{ "Category", "Mana" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentMana = { UE4CodeGen_Private::EPropertyClass::Float, "CurrentMana", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, CurrentMana), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentMana_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentMana_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxMana_MetaData[] = {
		{ "Category", "Mana" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxMana = { UE4CodeGen_Private::EPropertyClass::Float, "MaxMana", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, MaxMana), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxMana_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxMana_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseMana_MetaData[] = {
		{ "Category", "Mana" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseMana = { UE4CodeGen_Private::EPropertyClass::Float, "BaseMana", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseMana), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseMana_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseMana_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenItems_MetaData[] = {
		{ "Category", "Health Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenItems = { UE4CodeGen_Private::EPropertyClass::Float, "BonusHealthRegenItems", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusHealthRegenItems), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenItems_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenItems_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenStrength_MetaData[] = {
		{ "Category", "Health Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenStrength = { UE4CodeGen_Private::EPropertyClass::Float, "BonusHealthRegenStrength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusHealthRegenStrength), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenStrength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealthRegen_MetaData[] = {
		{ "Category", "Health Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealthRegen = { UE4CodeGen_Private::EPropertyClass::Float, "BaseHealthRegen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseHealthRegen), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealthRegen_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealthRegen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_HealthRegen_MetaData[] = {
		{ "Category", "Health Regen" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_HealthRegen = { UE4CodeGen_Private::EPropertyClass::Float, "HealthRegen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, HealthRegen), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_HealthRegen_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_HealthRegen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthItems_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthItems = { UE4CodeGen_Private::EPropertyClass::Float, "BonusHealthItems", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusHealthItems), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthItems_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthItems_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthStrength_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthStrength = { UE4CodeGen_Private::EPropertyClass::Float, "BonusHealthStrength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusHealthStrength), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthStrength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentHealth = { UE4CodeGen_Private::EPropertyClass::Float, "CurrentHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, CurrentHealth), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxHealth = { UE4CodeGen_Private::EPropertyClass::Float, "MaxHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, MaxHealth), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealth_MetaData[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealth = { UE4CodeGen_Private::EPropertyClass::Float, "BaseHealth", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseHealth), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealth_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAttackDamage_MetaData[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAttackDamage = { UE4CodeGen_Private::EPropertyClass::Float, "BaseAttackDamage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseAttackDamage), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAttackDamage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAttackDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_AbilityBonuses_MetaData[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_AbilityBonuses = { UE4CodeGen_Private::EPropertyClass::Float, "AbilityBonuses", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, AbilityBonuses), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_AbilityBonuses_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_AbilityBonuses_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinMaxGap_MetaData[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinMaxGap = { UE4CodeGen_Private::EPropertyClass::Float, "MinMaxGap", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, MinMaxGap), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinMaxGap_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinMaxGap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MainAttackDamage_MetaData[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MainAttackDamage = { UE4CodeGen_Private::EPropertyClass::Float, "MainAttackDamage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, MainAttackDamage), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MainAttackDamage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MainAttackDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaximumAttackDamage_MetaData[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaximumAttackDamage = { UE4CodeGen_Private::EPropertyClass::Float, "MaximumAttackDamage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, MaximumAttackDamage), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaximumAttackDamage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaximumAttackDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinimumAttackDamage_MetaData[] = {
		{ "Category", "Attack Damage" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinimumAttackDamage = { UE4CodeGen_Private::EPropertyClass::Float, "MinimumAttackDamage", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, MinimumAttackDamage), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinimumAttackDamage_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinimumAttackDamage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_PrimaryAttributePoints_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_PrimaryAttributePoints = { UE4CodeGen_Private::EPropertyClass::Float, "PrimaryAttributePoints", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, PrimaryAttributePoints), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_PrimaryAttributePoints_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_PrimaryAttributePoints_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusIntelligence_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusIntelligence = { UE4CodeGen_Private::EPropertyClass::Float, "BonusIntelligence", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusIntelligence), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusIntelligence_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusIntelligence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusAgility_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusAgility = { UE4CodeGen_Private::EPropertyClass::Float, "BonusAgility", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusAgility), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusAgility_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusAgility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusStrength_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusStrength = { UE4CodeGen_Private::EPropertyClass::Float, "BonusStrength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BonusStrength), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusStrength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseIntelligence_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseIntelligence = { UE4CodeGen_Private::EPropertyClass::Float, "BaseIntelligence", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseIntelligence), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseIntelligence_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseIntelligence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAgility_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAgility = { UE4CodeGen_Private::EPropertyClass::Float, "BaseAgility", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseAgility), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAgility_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAgility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseStrength_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseStrength = { UE4CodeGen_Private::EPropertyClass::Float, "BaseStrength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, BaseStrength), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseStrength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Intelligence_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Intelligence = { UE4CodeGen_Private::EPropertyClass::Float, "Intelligence", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, Intelligence), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Intelligence_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Intelligence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Agility_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Agility = { UE4CodeGen_Private::EPropertyClass::Float, "Agility", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, Agility), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Agility_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Agility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "Attributes" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
		{ "ToolTip", "Attributes" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Strength = { UE4CodeGen_Private::EPropertyClass::Float, "Strength", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000020005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, Strength), METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Strength_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction_MetaData[] = {
		{ "Category", "Primary Faction" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction = { UE4CodeGen_Private::EPropertyClass::Enum, "Faction", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, Faction), Z_Construct_UEnum_D_v0_EFaction, METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Primary Attribute" },
		{ "ModuleRelativePath", "Unit_Pawn.h" },
		{ "ToolTip", "VARIABLES" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type = { UE4CodeGen_Private::EPropertyClass::Enum, "Type", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AUnit_Pawn, Type), Z_Construct_UEnum_D_v0_EType, METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type_MetaData, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AUnit_Pawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRateModifiers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_TurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_DamageMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_EffectiveHP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusArmorAgility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseArmor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Armor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenItems,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaRegenIntelligence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseManaRegen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_ManaRegen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaItems,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusManaIntelligence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentMana,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxMana,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseMana,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenItems,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthRegenStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealthRegen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_HealthRegen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthItems,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusHealthStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_CurrentHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaxHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseHealth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAttackDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_AbilityBonuses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinMaxGap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MainAttackDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MaximumAttackDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_MinimumAttackDamage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_PrimaryAttributePoints,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusIntelligence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusAgility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BonusStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseIntelligence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseAgility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_BaseStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Intelligence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Agility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Faction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUnit_Pawn_Statics::NewProp_Type_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUnit_Pawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUnit_Pawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUnit_Pawn_Statics::ClassParams = {
		&AUnit_Pawn::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_AUnit_Pawn_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AUnit_Pawn_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AUnit_Pawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUnit_Pawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUnit_Pawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUnit_Pawn, 1391145786);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUnit_Pawn(Z_Construct_UClass_AUnit_Pawn, &AUnit_Pawn::StaticClass, TEXT("/Script/D_v0"), TEXT("AUnit_Pawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUnit_Pawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
