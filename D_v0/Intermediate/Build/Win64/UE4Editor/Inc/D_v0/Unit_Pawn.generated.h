// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef D_V0_Unit_Pawn_generated_h
#error "Unit_Pawn.generated.h already included, missing '#pragma once' in Unit_Pawn.h"
#endif
#define D_V0_Unit_Pawn_generated_h

#define D_v0_Source_D_v0_Unit_Pawn_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execComputeEffectiveHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeEffectiveHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeDamageMultiplier) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeDamageMultiplier(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeArmor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeArmor(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeTurnRate) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_modifier); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeTurnRate(Z_Param_modifier); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeManaRegen) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeManaRegen(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeMaxMana) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeMaxMana(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeHealthRegen) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeHealthRegen(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeMaxHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeMaxHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeAttackDamage) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeAttackDamage(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetPrimaryAttribute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetPrimaryAttribute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetAttribute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetAttribute(); \
		P_NATIVE_END; \
	}


#define D_v0_Source_D_v0_Unit_Pawn_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execComputeEffectiveHP) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeEffectiveHP(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeDamageMultiplier) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeDamageMultiplier(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeArmor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeArmor(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeTurnRate) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_modifier); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeTurnRate(Z_Param_modifier); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeManaRegen) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeManaRegen(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeMaxMana) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeMaxMana(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeHealthRegen) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeHealthRegen(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeMaxHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeMaxHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execComputeAttackDamage) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ComputeAttackDamage(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetPrimaryAttribute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetPrimaryAttribute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetAttribute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetAttribute(); \
		P_NATIVE_END; \
	}


#define D_v0_Source_D_v0_Unit_Pawn_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnit_Pawn(); \
	friend struct Z_Construct_UClass_AUnit_Pawn_Statics; \
public: \
	DECLARE_CLASS(AUnit_Pawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/D_v0"), NO_API) \
	DECLARE_SERIALIZER(AUnit_Pawn)


#define D_v0_Source_D_v0_Unit_Pawn_h_32_INCLASS \
private: \
	static void StaticRegisterNativesAUnit_Pawn(); \
	friend struct Z_Construct_UClass_AUnit_Pawn_Statics; \
public: \
	DECLARE_CLASS(AUnit_Pawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/D_v0"), NO_API) \
	DECLARE_SERIALIZER(AUnit_Pawn)


#define D_v0_Source_D_v0_Unit_Pawn_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnit_Pawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnit_Pawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnit_Pawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnit_Pawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnit_Pawn(AUnit_Pawn&&); \
	NO_API AUnit_Pawn(const AUnit_Pawn&); \
public:


#define D_v0_Source_D_v0_Unit_Pawn_h_32_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnit_Pawn(AUnit_Pawn&&); \
	NO_API AUnit_Pawn(const AUnit_Pawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnit_Pawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnit_Pawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUnit_Pawn)


#define D_v0_Source_D_v0_Unit_Pawn_h_32_PRIVATE_PROPERTY_OFFSET
#define D_v0_Source_D_v0_Unit_Pawn_h_29_PROLOG
#define D_v0_Source_D_v0_Unit_Pawn_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	D_v0_Source_D_v0_Unit_Pawn_h_32_PRIVATE_PROPERTY_OFFSET \
	D_v0_Source_D_v0_Unit_Pawn_h_32_RPC_WRAPPERS \
	D_v0_Source_D_v0_Unit_Pawn_h_32_INCLASS \
	D_v0_Source_D_v0_Unit_Pawn_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define D_v0_Source_D_v0_Unit_Pawn_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	D_v0_Source_D_v0_Unit_Pawn_h_32_PRIVATE_PROPERTY_OFFSET \
	D_v0_Source_D_v0_Unit_Pawn_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	D_v0_Source_D_v0_Unit_Pawn_h_32_INCLASS_NO_PURE_DECLS \
	D_v0_Source_D_v0_Unit_Pawn_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID D_v0_Source_D_v0_Unit_Pawn_h


#define FOREACH_ENUM_EFACTION(op) \
	op(EFaction::Faction_Dire) \
	op(EFaction::Faction_Radiant) \
	op(EFaction::Faction_Neutral) 
#define FOREACH_ENUM_ETYPE(op) \
	op(EType::Type_Strength) \
	op(EType::Type_Agility) \
	op(EType::Type_Intelligence) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
