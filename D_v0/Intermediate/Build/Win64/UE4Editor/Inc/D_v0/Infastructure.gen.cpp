// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "D_v0/Infastructure.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInfastructure() {}
// Cross Module References
	D_V0_API UClass* Z_Construct_UClass_AInfastructure_NoRegister();
	D_V0_API UClass* Z_Construct_UClass_AInfastructure();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_D_v0();
// End Cross Module References
	void AInfastructure::StaticRegisterNativesAInfastructure()
	{
	}
	UClass* Z_Construct_UClass_AInfastructure_NoRegister()
	{
		return AInfastructure::StaticClass();
	}
	struct Z_Construct_UClass_AInfastructure_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AInfastructure_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_D_v0,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AInfastructure_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Infastructure.h" },
		{ "ModuleRelativePath", "Infastructure.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AInfastructure_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AInfastructure>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AInfastructure_Statics::ClassParams = {
		&AInfastructure::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AInfastructure_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AInfastructure_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AInfastructure()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AInfastructure_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AInfastructure, 2546101432);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AInfastructure(Z_Construct_UClass_AInfastructure, &AInfastructure::StaticClass, TEXT("/Script/D_v0"), TEXT("AInfastructure"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AInfastructure);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
