// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef D_V0_Items_generated_h
#error "Items.generated.h already included, missing '#pragma once' in Items.h"
#endif
#define D_V0_Items_generated_h

#define D_v0_Source_D_v0_Items_h_12_RPC_WRAPPERS
#define D_v0_Source_D_v0_Items_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define D_v0_Source_D_v0_Items_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAItems(); \
	friend struct Z_Construct_UClass_AItems_Statics; \
public: \
	DECLARE_CLASS(AItems, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/D_v0"), NO_API) \
	DECLARE_SERIALIZER(AItems)


#define D_v0_Source_D_v0_Items_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAItems(); \
	friend struct Z_Construct_UClass_AItems_Statics; \
public: \
	DECLARE_CLASS(AItems, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/D_v0"), NO_API) \
	DECLARE_SERIALIZER(AItems)


#define D_v0_Source_D_v0_Items_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AItems(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AItems) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItems); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItems); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItems(AItems&&); \
	NO_API AItems(const AItems&); \
public:


#define D_v0_Source_D_v0_Items_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AItems(AItems&&); \
	NO_API AItems(const AItems&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AItems); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AItems); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AItems)


#define D_v0_Source_D_v0_Items_h_12_PRIVATE_PROPERTY_OFFSET
#define D_v0_Source_D_v0_Items_h_9_PROLOG
#define D_v0_Source_D_v0_Items_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	D_v0_Source_D_v0_Items_h_12_PRIVATE_PROPERTY_OFFSET \
	D_v0_Source_D_v0_Items_h_12_RPC_WRAPPERS \
	D_v0_Source_D_v0_Items_h_12_INCLASS \
	D_v0_Source_D_v0_Items_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define D_v0_Source_D_v0_Items_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	D_v0_Source_D_v0_Items_h_12_PRIVATE_PROPERTY_OFFSET \
	D_v0_Source_D_v0_Items_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	D_v0_Source_D_v0_Items_h_12_INCLASS_NO_PURE_DECLS \
	D_v0_Source_D_v0_Items_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID D_v0_Source_D_v0_Items_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
