// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Unit_Pawn.generated.h"


UENUM(BlueprintType)
enum class EType : uint8
{
	Type_Strength 		UMETA(DisplayName = "Strength"),
	Type_Agility 		UMETA(DisplayName = "Agility"),
	Type_Intelligence 	UMETA(DisplayName = "Intelligence")

};


UENUM(BlueprintType)
enum class EFaction : uint8
{
	Faction_Dire 		UMETA(DisplayName = "Dire"),
	Faction_Radiant 		UMETA(DisplayName = "Radiant"),
	Faction_Neutral 		UMETA(DisplayName = "Neutral")

};

UCLASS()
class D_V0_API AUnit_Pawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AUnit_Pawn();
	/*					VARIABLES					*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Primary Attribute")
		EType Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Primary Faction")
		EFaction Faction;
	//Attributes

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float Strength;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float Agility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float Intelligence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		float BaseStrength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		float BaseAgility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		float BaseIntelligence;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float BonusStrength;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float BonusAgility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float BonusIntelligence;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attributes")
		float PrimaryAttributePoints;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attack Damage")
		float MinimumAttackDamage;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attack Damage")
		float MaximumAttackDamage;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attack Damage")
		float MainAttackDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack Damage")
		float MinMaxGap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack Damage")
		float AbilityBonuses;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack Damage")
		float BaseAttackDamage;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float BaseHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CurrentHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float BonusHealthStrength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float BonusHealthItems;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health Regen")
		float HealthRegen;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health Regen")
		float BaseHealthRegen;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Health Regen")
		float BonusHealthRegenStrength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health Regen")
		float BonusHealthRegenItems;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mana")
		float BaseMana;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mana")
		float MaxMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float CurrentMana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float BonusManaIntelligence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana")
		float BonusManaItems;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mana Regen")
		float ManaRegen;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana Regen")
		float BaseManaRegen;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mana Regen")
		float BonusManaRegenIntelligence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mana Regen")
		float BonusManaRegenItems;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Armor")
		float Armor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor")
		float BaseArmor;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Armor")
		float BonusArmorAgility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor")
		float BonusArmor;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Armor")
		float EffectiveHP;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Armor")
		float DamageMultiplier;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Turn Rate")
		float TurnRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Turn Rate")
		float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Turn Rate")
		float TurnRateModifiers;


	/*					FUNCTIONS					*/

	//UFUNCTION(BlueprintCallable, Category = "Armor")
	//	void ComputeMainArmor();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
		void SetAttribute();

	UFUNCTION(BlueprintCallable, Category = "Primary Attribute")
		void SetPrimaryAttribute();

	UFUNCTION(BlueprintCallable, Category = "Attack Damage")
		void ComputeAttackDamage();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void ComputeMaxHealth();

	UFUNCTION(BlueprintCallable, Category = "Health Regen")
		void ComputeHealthRegen();

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void ComputeMaxMana();

	UFUNCTION(BlueprintCallable, Category = "Mana Regen")
		void ComputeManaRegen();

	UFUNCTION(BlueprintCallable, Category = "Turn Rate")
		void ComputeTurnRate(float modifier);

	UFUNCTION(BlueprintCallable, Category = "Armor")
		void ComputeArmor();

	UFUNCTION(BlueprintCallable, Category = "Armor")
		void ComputeDamageMultiplier();

	UFUNCTION(BlueprintCallable, Category = "Armor")
		void ComputeEffectiveHP();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
