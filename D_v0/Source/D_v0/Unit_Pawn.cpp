// Fill out your copyright notice in the Description page of Project Settings.

#include "Unit_Pawn.h"

// Sets default values
AUnit_Pawn::AUnit_Pawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AUnit_Pawn::SetAttribute()
{
	Strength = BaseStrength + BonusStrength;
	Agility = BaseAgility + BonusStrength;
	Intelligence = BaseIntelligence + BonusIntelligence;
}

void AUnit_Pawn::SetPrimaryAttribute()
{

	SetAttribute();

	if (Type == EType::Type_Strength)
	{
		PrimaryAttributePoints = Strength;
	}

	else if (Type == EType::Type_Agility)
	{
		PrimaryAttributePoints = Agility;
	}

	else if (Type == EType::Type_Intelligence)
	{
		PrimaryAttributePoints = Intelligence;
	}

	ComputeAttackDamage();
	ComputeMaxHealth();

	//Remove this in the future
	CurrentHealth = MaxHealth;

	ComputeHealthRegen();
	ComputeMaxMana();
	ComputeManaRegen();
	ComputeArmor();
	ComputeDamageMultiplier();
	ComputeEffectiveHP();
}

void AUnit_Pawn::ComputeAttackDamage()
{
	MainAttackDamage = BaseAttackDamage + PrimaryAttributePoints + AbilityBonuses;
}

void AUnit_Pawn::ComputeMaxHealth()
{
	BaseHealth = 200;
	BonusHealthStrength = Strength * 20;
	
	MaxHealth = BaseHealth + BonusHealthStrength + BonusHealthItems;
}

void AUnit_Pawn::ComputeHealthRegen()
{
	BonusHealthRegenStrength = Strength * 0.1;
	HealthRegen = BaseHealthRegen + BonusHealthRegenStrength + BonusHealthRegenItems;
}

void AUnit_Pawn::ComputeMaxMana()
{
	BaseMana = 75;
	BonusManaIntelligence = Intelligence * 12;

	MaxMana = BaseMana + BonusManaIntelligence + BonusManaItems;
}

void AUnit_Pawn::ComputeManaRegen()
{
	BonusManaRegenIntelligence = Intelligence * 0.05;
	ManaRegen = BaseManaRegen + BonusManaRegenIntelligence + BonusManaRegenItems;
}

void AUnit_Pawn::ComputeTurnRate(float modifier)
{
	TurnRateModifiers = modifier;
	TurnRate = BaseTurnRate + TurnRateModifiers;
}

void AUnit_Pawn::ComputeArmor()
{
	BonusArmorAgility = Agility * 0.16;

	Armor = BaseArmor + BonusArmorAgility;
	Armor += BonusArmor;
}

void AUnit_Pawn::ComputeDamageMultiplier()
{
	DamageMultiplier = 1 - ((0.052 * Armor) / (0.9 + 0.048 * (abs(Armor))));
}

void AUnit_Pawn::ComputeEffectiveHP()
{
	EffectiveHP = CurrentHealth / DamageMultiplier;
}

// Called when the game starts or when spawned
void AUnit_Pawn::BeginPlay()
{
	Super::BeginPlay();
	
	SetPrimaryAttribute();
	ComputeTurnRate(0);
}

// Called every frame
void AUnit_Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AUnit_Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

