// Fill out your copyright notice in the Description page of Project Settings.

#include "Infastructure.h"

// Sets default values
AInfastructure::AInfastructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AInfastructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInfastructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

